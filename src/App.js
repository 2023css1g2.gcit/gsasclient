import React,{useState} from 'react';
import { Route,createBrowserRouter,createRoutesFromElements,RouterProvider,Navigate } from 'react-router-dom';

import Sidebar from './components/Sidebar';
import Allocation from './pages/Allocation';
import Allocationrun from './pages/Allocationrun';
import Report from './pages/Report';
import Dashboard from './pages/Dashboard';
import Fileupload from './pages/Fileupload';
import Academy from './pages/Academy';
import Profile from './pages/Profile';
import ChangePassword from './components/ChangePassword';
import Login from './pages/Login';
import ForgotPassword from './components/ForgotPassword';

function App(props) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
  }

  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route>
        <Route path='/' element = {isLoggedIn?<Navigate to = "/Gyalsung"/>:<Login handleAdminLogin = {handleLogin}/>}>

        </Route>

        <Route path='forgotpassword' element = {<ForgotPassword/>}>

        </Route>

        <Route path='changepassword' element = {<ChangePassword/>}>

        </Route>

        <Route path='Gyalsung' element = {isLoggedIn?<Sidebar handleLogout = {handleLogout}/>:<Navigate to ="/"/>}>
          <Route index element={<Dashboard/>} />
          <Route path='fileupload' element={<Fileupload/>} />
          <Route path='allocation' element={<Allocation/>} />
          <Route path='academy' element={<Academy/>} />
          <Route path='allocationrun' element={<Allocationrun/>} />
          <Route path='report' element={<Report/>} />
          <Route path='profile' element={<Profile/>} />

        </Route>
      </Route>
    )
  )

  return (
    <RouterProvider router = {router}/>
  );
}

export default App;