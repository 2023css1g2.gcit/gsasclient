import React, { useEffect, useState } from 'react';
import Papa from 'papaparse';
import ReactPaginate from 'react-paginate';
import graduateCap from '../../img/game-icons_graduate-cap.png';
import Cookies from 'js-cookie';

function Preview({ setPreviousStep, fileName, selectedFile, setCompleteStep }) {

  const [token,setToken] = useState('');

  const itemsPerPage = 10;
  const [currentPage, setCurrentPage] = useState(0);
  const [csvData, setCsvData] = useState(null);

  useEffect(() => {
    if (selectedFile) {
      console.log("selected file ", selectedFile);
      Papa.parse(selectedFile, {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        complete: (result) => {
          setCsvData(result.data);
        },
        error: (error) => {
          console.error('Error parsing CSV ', error);
        },
      });
    }
  }, [selectedFile]);

  useEffect(() => {
    const token = Cookies.get('auth_token');
    setToken(token);
  },[])

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  const startIndex = currentPage * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentData = csvData && csvData.slice(startIndex, endIndex);

  const handleComplete = async () => {
    const formData = new FormData();
    formData.append('file',selectedFile);
    formData.append('fileName',fileName);

    await fetch('http://localhost:8000/api/fileUpload',{
      method:'POST',
      headers: {
        'Authorization':`Bearer ${token}`
      },
      body:formData,
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.log("eror ",error);
    })
  };

  return (
    <div className="grid-container" style={{display: 'grid', gridTemplateColumns: '3fr 1fr' }}>
      <div>
        <h1 style={{padding:'0 20px 0 20px'}}>{fileName}</h1>
        <div className="left-panel" style={{ borderRight: '1px solid #ccc', padding: '20px',maxHeight: 'calc(100vh - 200px)', overflowY: 'auto' }}>
          <div className="container shadow p-3 bg-body rounded">
            {currentData && (
              <div className="table-responsive">
                <table className="table">
                  <thead className="table-heading">
                    <tr>
                      {Object.keys(currentData[0]).map((header) => (
                        <th key={header}>{header}</th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {currentData.map((row, index) => (
                      <tr key={index}>
                        {Object.values(row).map((value, colIndex) => (
                          <td key={colIndex}>{value}</td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            )}
            {csvData && (
              <div className="d-flex justify-content-between align-items-center mb-3 mt-2">
                <ReactPaginate
                  previousLabel={false}
                  nextLabel={false}
                  breakLabel={'......'}
                  pageCount={Math.ceil(csvData.length / itemsPerPage)}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={handlePageChange}
                  containerClassName={'pagination'}
                  activeClassName={'active'}
                  pageClassName={'paginate-page'}
                  previousClassName={'paginate-previous'}
                  nextClassName={'paginate-next'}
                />
              </div>
            )}
          </div>
          <div className='d-flex justify-content-between mt-5' style={{marginBottom:100}}>
            <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
            <button className="btn" onClick={handleComplete} style={{ backgroundColor: '#F04A00', color: 'white' }}>
              Complete
            </button>
          </div>
        </div>
      </div>
      <div className="right-panel" style={{ padding: '10px',marginRight:'-20px' }}>
        <form action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px' }}>
          <p className='text-center'><img src={graduateCap} alt='graduate cap' style={{width:'54px',height:'54px'}}/></p>
          <h5 className='text-center text-light'>Academy Enlistee</h5>
          <div className='mt-3'>
            <label className='text-light'>Search by CID</label>
            <input
              name='cid'
              className='form-control'
            />
          </div>
          <div className='mt-3'>
            <label className='text-light'>Gender</label>
            <input className='form-control' name='sex' />
          </div>
          <div className='mt-4 mb-4 text-end'>
            <button className='btn form-control' style={{ backgroundColor: '#F04A00', color: 'white' }}>Search</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Preview;
