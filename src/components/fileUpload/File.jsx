import React, { useEffect } from 'react';
import './../../style/fileupload/fileupload.css';
import { BsFiletypeCsv, BsFileEarmarkText } from 'react-icons/bs';
import { useState, useRef } from 'react';

function File({ selectedFile, onFileSelect, setNextStep, setFileUploadedName, fileName, clearInput }) {
  const inputRef = useRef();
  const [isDragOver, setIsDragOver] = useState(false);
  const [noInput, setNoInput] = useState(false);

  const winHeight = window.innerHeight;

  const handleDragOver = (event) => {
    event.preventDefault();
    setIsDragOver(true);
  }

  const handleDrop = (event) => {
    event.preventDefault();
    onFileSelect(event.dataTransfer.files[0]);
    setIsDragOver(false);
  }

  const handleDragLeave = () => {
    setIsDragOver(false);
  }

  const handleSubmit = () => {
    if (!selectedFile || !fileName) {
      setNoInput(true);
    } else {
      setNextStep();
    }
  }

  useEffect(() => {
    if (selectedFile && fileName) {
      setNoInput(false);
    }
  }, [selectedFile, fileName])

  console.log(selectedFile);
  console.log(fileName);

  return (
    <div style={{display:'flex', justifyContent: 'center',alignItems:'center'}} >
      <div className='fileupload__container'>
        <div className={`file__drag__and__drop__container ${isDragOver ? 'drag__over' : ''}`}
          onDragOver={handleDragOver}
          onDrop={handleDrop}
          onDragLeave={handleDragLeave}
        >
          <div className='file__drag__and__drop__innercontainer'>
            <BsFiletypeCsv size={70} />
            <input type="file" accept=".csv" onChange={(event) => onFileSelect(event.target.files[0])} hidden ref={inputRef} />
            <p className='file__text'>Drag and Drop Or <span
              className='browse__file'
              onClick={() => inputRef.current.click()}
            >Browse</span> a file</p>
          </div>
        </div>
        {selectedFile &&
          <div className='file__name'>
            <BsFileEarmarkText size={30} />
            <div>{selectedFile.name}</div>
          </div>
        }
        <div>
          <label htmlFor="filename">File Name</label>
          <div>
            <input type="text" className='form-control filename' style={{ width: 281, height: 40, paddingLeft: 10 }} id='filename'
              onChange={(e) => setFileUploadedName(e.target.value)}
              value={fileName}
            />
            {noInput &&
              <p style={{ color: 'red', marginTop: 2 }}>File or filename is missing!</p>
            }
          </div>
        </div>

        <div className='button__container'>
          <button type="button" className='btn btn-outline-primary reset__button no-hover'
            onClick={clearInput}
          >Reset</button>
          <button type="button" className='btn btn-outline-primary upload__button'
            onClick={handleSubmit}
          >Next</button>
        </div>
      </div>
    </div>
  )
}

export default File;
