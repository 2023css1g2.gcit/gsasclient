
import React from 'react'
import { Card, Row, Col } from 'react-bootstrap';
import '../../style/Profile/profile.css';

const Overview = ({handlePageChange,currentPage}) => {

    console.log("current page from overview ",currentPage);

  return (
    <div className="container">
      <Row className="ProfilePic">
        <Col md={4} className="ProfilePic">
          <Card id="Picture" className="d-flex align-items-center justify-content-center">
           <div></div>
            <div className="card-body">
              <h7 className="card-title">Sonam Tobden</h7>
            </div>
          </Card>
        </Col>
        <Col md={8}>
          <Card className='Card'>
            <div className="overview">
            <div className="card-body">
            <table className="table">
                <tbody>
                  <tr>
                    
                    <td> <div onClick={handlePageChange} href="Profile"  className={`${currentPage === true?"overviewP text-center":"text-center"}`} >Overview</div></td>
                    <td> <div onClick={handlePageChange} href="ChangePasswordProfile"  className='text-center' >Change password</div></td>
                   </tr>
                  
                </tbody>
              </table>
            </div>
           
                   
          
                  </div>
            <div className="card-header">Profile Details</div>
            <div className="card-body">
              <table className="table">
                <tbody>
                  <tr>
                    <td>Full Name</td>
                    <td>Sonam Tobden</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>sonamt251@gmail.com</td>
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Overview;
