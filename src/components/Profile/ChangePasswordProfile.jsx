
import React, { useState } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import '../../style/Profile/changepw.css';
import {AiFillEyeInvisible} from 'react-icons/ai';
import {AiFillEye} from 'react-icons/ai';
import { useLocation,useNavigate } from 'react-router-dom';

const ChangePasswordProfile = ({handlePageChange,currentPage}) => {


  console.log("current page from changepassword",currentPage);

  const {state} = useLocation();
  const navigate = useNavigate();

  const [password,setPassword] = useState('');
  const [confirmpassword,setconfirmpassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [eyeOpen1,setEyeOpen1] = useState(false);
  const [eyeOpen2,setEyeOpen2] = useState(false);
  const [inputone,setInputOne] = useState(false);
  const [inputtwo,setInputTwo] = useState(false);

  const [error,setError] = useState("");
  
  const [pin, setPin] = useState(['', '', '', '']);

  const handlePinChange = (index, value) => {
    if (/^\d*$/.test(value) && value.length <= 1) {
        const newPin = [...pin];
        newPin[index] = value;
        setPin(newPin);
        
        // Move to the next input if a digit is entered
        if (value && index < 3) {
            document.getElementById(`code${index + 2}`).focus();
        }
    }
  };

  const handlePasswordReset = async (e) => {
    e.preventDefault();
    setIsLoading(true)
    const pinString = pin.join('');
    if (pinString.length !== 4) {
      setError("PinCode is not valid!");
      return;
    }
    
    if (password !== confirmpassword) {
      setError("Password doesn't match!");
      return;
    }

    try {
      const response = await fetch('http://localhost:8000/api/resetPassword', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "email":state,"token":pinString,"password":password })
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        navigate('/login');
      } else {
        const data = await response.json();
        setError(data.message)
      }

    } catch (error) {
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  }

  console.log(password,confirmpassword);
  console.log(pin)
  console.log("email ",state);

  return (
    <div className="container">
      <Row className="ProfilePic">
        <Col md={4} className="ProfilePic">
          <Card id="Picture" className="d-flex align-items-center justify-content-center">
            <div></div>
            <div className="card-body">
              <h7 className="card-title">Sonam Tobden</h7>
            </div>
          </Card>
        </Col>
        <Col md={8}>
          <Card className='Card'>
            <div className="overview">
            <div className="card-body">
            <table className="table">
                <tbody>
                  <tr>
                    
                    <td> <div onClick={handlePageChange} className='text-center' >Overview</div></td>
                    <td> <div onClick={handlePageChange} className={`${currentPage === false?"changepw text-center":"text-center"}`} >Change password</div></td>
                   </tr>
                  
                </tbody>
              </table>
            </div>
           
                   
          
                  </div>
            <div className="card-body">
              <table className="table">
                <tbody>
                  <tr>
                    <td><label className='currentPassword__label' htmlFor="password">Current Password</label></td>
                    <td>
                      <div className="currentPassword__text__area">
                        <input
                          type={inputtwo?"text":"password"}
                          id="confirmpassword"
                          name="password"
                          className="currentPassword__text__input"
                          onChange={(e) => setconfirmpassword(e.target.value)}
                        />
                        {eyeOpen2 &&
                        <AiFillEyeInvisible size={20} className="login__eye__closed" 
                        onClick={() => {
                          setEyeOpen2(!eyeOpen2)
                          setInputTwo(!inputtwo)
                        }}
                      /> 
                    }
                    {!eyeOpen2 &&
                    <AiFillEye size={20} className="login__eye__closed" 
                        onClick={()=> {
                        setEyeOpen2(!eyeOpen2);
                        setInputTwo(!inputtwo)
                      }}
                    />
                    }   
                  </div>
              
              </td>
                  </tr>
                  <tr>
                  <td><label className='currentPassword__label' htmlFor="password">New Password</label></td>
                    <td>
                      <div className="currentPassword__text__area">
                        <input
                          type={inputtwo?"text":"password"}
                          id="confirmpassword"
                          name="password"
                          className="currentPassword__text__input"
                          onChange={(e) => setconfirmpassword(e.target.value)}
                        />
                        {eyeOpen2 &&
                        <AiFillEyeInvisible size={20} className="login__eye__closed" 
                        onClick={() => {
                          setEyeOpen2(!eyeOpen2)
                          setInputTwo(!inputtwo)
                        }}
                      /> 
                    }
                    {!eyeOpen2 &&
                    <AiFillEye size={20} className="login__eye__closed" 
                        onClick={()=> {
                        setEyeOpen2(!eyeOpen2);
                        setInputTwo(!inputtwo)
                      }}
                    />
                    }   
                  </div>
              
              </td>
                  </tr>

                  <tr>
                  <td><label className='currentPassword__label' htmlFor="password">Re-enter New Password</label></td>
                    <td>
                      <div className="currentPassword__text__area">
                        <input
                          type={inputtwo?"text":"password"}
                          id="confirmpassword"
                          name="password"
                          className="currentPassword__text__input"
                          onChange={(e) => setconfirmpassword(e.target.value)}
                        />
                        {eyeOpen2 &&
                        <AiFillEyeInvisible size={20} className="login__eye__closed" 
                        onClick={() => {
                          setEyeOpen2(!eyeOpen2)
                          setInputTwo(!inputtwo)
                        }}
                      /> 
                    }
                    {!eyeOpen2 &&
                    <AiFillEye size={20} className="login__eye__closed" 
                        onClick={()=> {
                        setEyeOpen2(!eyeOpen2);
                        setInputTwo(!inputtwo)
                      }}
                    />
                    }   
                  </div>
              
              </td>
                  </tr>
                  
                  <tr>
                   <td></td>
                    <td>
                      <div style={{ textAlign: 'center' }}>

                        {isLoading ? (
                        <button className="setPassword_btn" disabled>
                        <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                        <span className='ms-2'>Changing...</span>
                      </button>
                    ):(
                  <input
                  type="submit"
                  value="Confirm"
                  className="setPassword_btn"
                  />
                  )
                  }

                </div>

                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default ChangePasswordProfile;
