import React from 'react'
import { useState,useEffect } from 'react';
import game from "../img/game-icons_graduate-cap.png";
import vertor from "../img/Vector.png";
import vertor1 from "../img/Vector (1).png";
import vertor2 from "../img/Vector (2).png"
import "../style/Enlisteedetail.css"
export default function Enlisteedetail() {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth - 295 - 30);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth - 295 - 38);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className='enlistee__container' style={{width:`${windowWidth}px`}}>
        <div className='enlistee__detail' >
            <div className='img__container' >
                <img src={game} alt="Game" />
            </div>
            <div className='text__container' >
                <div className='inner__container'>
                    <p style={{marginTop:20, color: "#F04A00", fontWeight: 'bold', fontSize:24}}>4</p>
                    <p style={{marginTop: -20}}>Total Academy</p>
                </div>
            </div>
        </div>
        <div className='enlistee__detail' >
            <div className='img__container' >
                <img src={vertor} alt="Vertor" />
            </div>
            <div className='text__container' >
                <div className='inner__container'>
                    <p style={{marginTop:20, color: "#F04A00", fontWeight: 'bold',fontSize: 24}}>12000</p>
                    <p style={{marginTop:-20}}>Total Enlistees</p>
                </div>
            </div>
        </div>
        <div className='enlistee__detail' >
            <div className='img__container' >
                <img src={vertor1} alt="Vertor1" />
            </div>
            <div className='text__container' >
               <div className='inner__container'> 
                    <p style={{marginTop:20, color: "#F04A00", fontWeight: 'bold', fontSize: 24}}>6000</p>
                    <p style={{marginTop:-20}}>Total Male</p>
               </div>
            </div>
        </div>
        <div className='enlistee__detail' >
            <div className='img__container' >
                <img src={vertor2} alt="Vertor2" />
            </div>
            <div className='text__container'>
                <div className='inner__container'>
                    <p style={{marginTop:20, color: "#F04A00", fontWeight: 'bold', fontSize: 24}}>6000</p>
                    <p style={{marginTop: -20}}>Total Female</p>
                </div>
            </div>
        </div>
    </div>
  )
}
