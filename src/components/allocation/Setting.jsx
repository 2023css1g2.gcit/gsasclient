import React, { useState } from 'react'
import GeneralSetting from '../setting/GeneralSetting';
import Pemathang from '../pemathang/Pemathang';
import Jamtsholing from '../jamtsholing/Jamtsholing';
import Khotokha from '../khotokha/Khotokha';
import { useSelector } from 'react-redux';
import { useStartAllocationMutation } from '../../store';

function Setting({setNextStep,setPreviousStep}) {

  const [startAllocation,{isLoading,isError,data}] = useStartAllocationMutation();

  const gyalpozhingData = useSelector((state) => state.gyalpozhing);
  const fileData = useSelector((state) => state.file);

  console.log("from setting ",gyalpozhingData);
  console.log("from setting ", fileData);

  const handleSubmit = () => {
    const allocationData = {
      name:fileData.name,
      year:fileData.year,
      gyalpozhingData
    }
    console.log(allocationData);

    startAllocation(allocationData)
    .unwrap()
    .then((response) => {
      console.log(response);
    })
  }

  const [academyIndex,setAcademyIndex] = useState(0);

  return (
    <div>
        <div className='d-flex justify-content-between'>
          <div className='border border-dark px-3' style={{cursor:'pointer'}} onClick={() => setAcademyIndex(0)}><h5>Gyalpozhing Academy</h5></div>
          <div className='border border-dark px-3' style={{cursor:'pointer'}} onClick={() => setAcademyIndex(1)}><h5>Pemathang Academy</h5></div>
          <div className='border border-dark px-3' style={{cursor:'pointer'}} onClick={() => setAcademyIndex(2)}><h5>Jamtsholing Academy</h5></div>
          <div className='border border-dark px-3' style={{cursor:'pointer'}} onClick={() => setAcademyIndex(3)}><h5>Khotokha Academy</h5></div>
        </div>
        {academyIndex === 0 &&
          <GeneralSetting/>
        }

        {academyIndex === 1 &&
          <Pemathang/>
        }
        {academyIndex === 2 &&
          <Jamtsholing/>
        }
        {academyIndex === 3 &&
          <Khotokha/>
        }
        <button onClick={setPreviousStep}>Previous</button>
        <button onClick={handleSubmit}>Run</button>
    </div>
  )
}

export default Setting
