import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import ReactPaginate from 'react-paginate';
import graduateCap from '../../img/game-icons_graduate-cap.png';
import { addStudentData } from '../../store';
import { useDispatch } from 'react-redux';
import { useAddStudentMutation } from '../../store';

export default function Preview({setNextStep,setPreviousStep}) {

  const dispatch = useDispatch();

  const [addStudent, {isLoading,isError,data}] = useAddStudentMutation();

  const {name,year,fileContent,studentData} = useSelector((state) => state.file);

  console.log(name,year,fileContent,studentData.length);

  const itemsPerPage = 10;
  const [currentPage,setCurrentPage] = useState(0);
  const [csvData,setCsvData] = useState([]);
  const [contentHeaders,setContentHeaders] = useState([]);

  useEffect(() => {
    const fileData = fileContent;

    console.log("data ",fileContent);

    const lines = fileData.split('\n');
    const headers = lines[0].split(',').map(header => header.replace(/\r/g, ''));
    setContentHeaders(headers);
    const parsedData = [];

    for (let i = 1; i < lines.length; i++) {
        const values = lines[i].split(',');
        if (values.length === headers.length) {
            const rowData = {};
            headers.forEach((header,index) => {
                rowData[header] = values[index]
            })
            parsedData.push(rowData);
        }
    }

    setCsvData(parsedData);
    dispatch(addStudentData(parsedData));
    },[fileContent]);

    const handlePageChange = ({ selected }) => {
      setCurrentPage(selected);
    };

    const offset = currentPage * itemsPerPage;
    const currentData = csvData && csvData.slice(offset, offset + itemsPerPage);
  
    const handleStudentDataSubmit = () => {

      const data = {
        studentData:studentData
      }

      console.log(data);

      addStudent(data) 
      .unwrap()
      .then((response) => {
        if (response.status) {
          setNextStep();
        }
      })
    }

    console.log("student data ",csvData.length);
    console.log("Student data from ",studentData);

  return (
    <div className="grid-container" style={{display: 'grid', gridTemplateColumns: '3fr 1fr' }}>
      <div>
        <h1 style={{padding:'0 20px 0 20px'}}>{name}</h1>
        <div className="left-panel" style={{ borderRight: '1px solid #ccc', padding: '20px',maxHeight: 'calc(100vh - 200px)', overflowY: 'auto' }}>
          <div className="container shadow p-3 bg-body rounded">
          {currentData && currentData.length > 0 ? ( // Check if currentData is defined and not empty
              <div className="table-responsive">
                <table className="table">
                  <thead className="table-heading">
                    <tr>
                      {contentHeaders.map((header) => (
                        <th key={header}>{header}</th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {currentData.map((row, index) => (
                      <tr key={index}>
                        {Object.values(row).map((value, colIndex) => (
                          <td key={colIndex}>{value}</td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            ) : (
              <p>No data available.</p>
            )}
            {csvData && (
              <div className="d-flex justify-content-between align-items-center mb-3 mt-2">
                <ReactPaginate
                  previousLabel={'previous'}
                  nextLabel={'next'}
                  breakLabel={'......'}
                  pageCount={Math.ceil(csvData.length / itemsPerPage)}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={handlePageChange}
                  containerClassName={'pagination'}
                  activeClassName={'active'}
                  pageClassName={'paginate-page'}
                  previousClassName={'paginate-previous'}
                  nextClassName={'paginate-next'}
                />
              </div>
            )}
          </div>
          <div className='d-flex justify-content-between mt-5' style={{marginBottom:100}}>
            <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
            <button className="btn" onClick={handleStudentDataSubmit} style={{ backgroundColor: '#F04A00', color: 'white' }}>
              Next
            </button>
          </div>
          <div>
            {isLoading &&
              <div>Student data storing... </div>
            }
          </div>
        </div>
      </div>
      <div className="right-panel" style={{ padding: '10px',marginRight:'-20px' }}>
        <form action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px' }}>
          <p className='text-center'><img src={graduateCap} alt='graduate cap' style={{width:'54px',height:'54px'}}/></p>
          <h5 className='text-center text-light'>Academy Enlistee</h5>
          <div className='mt-3'>
            <label className='text-light'>Search by CID</label>
            <input
              name='cid'
              className='form-control'
            />
          </div>
          <div className='mt-3'>
            <label className='text-light'>Gender</label>
            <input className='form-control' name='sex' />
          </div>
          <div className='mt-4 mb-4 text-end'>
            <button className='btn form-control' style={{ backgroundColor: '#F04A00', color: 'white' }}>Search</button>
          </div>
        </form>
      </div>
    </div>
  )
}
