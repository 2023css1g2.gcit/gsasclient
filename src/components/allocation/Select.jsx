import React, { useEffect, useState } from 'react';
import {MdDeleteForever} from 'react-icons/md';
import '../../style/allocationStyle/SelectFile.css';
import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css';
import {useFetchFilesQuery} from '../../store';
import ReactPaginate from 'react-paginate';
import { updateData } from '../../store';
import { useDispatch } from 'react-redux';

export default function Select({setNextStep,setPreviousStep}) {

    const dispatch = useDispatch();

    const {data,error,isLoading, refetch} = useFetchFilesQuery();

    // const [fileContent,setFileContent] = useState([]);
    // const [currentPage, setCurrentPage] = useState(0);
    // const [Contentheaders,setContentHeaders] = useState([]);
    // const itemsPerPage = 10;

    const handleAllocationStart = () => {
        if (data && data.fileDetails.length > 0) {
            console.log(data);
            console.log(data.fileDetails.length)
            const selectedFile = {
                fileName:data.fileDetails[0].filename,
                fileContent:data.fileDetails[0].fileContent,
                year:2023
             }
     
             dispatch(updateData(selectedFile));
             setNextStep();
        } else {
            console.log("File not selected");
        }
    }

    useEffect(() => {
        refetch();
    },[])


    // useEffect(() => {
    //     const fileData = data?.fileDetails[0]?.fileContent;

    //     console.log("data ",data);

    //     const lines = fileData.split('\n');
    //     const headers = lines[0].split(',');
    //     setContentHeaders(headers);
    //     const parsedData = [];

    //     for (let i = 1; i < lines.length; i++) {
    //         const values = lines[i].split(',');
    //         if (values.length === headers.length) {
    //             const rowData = {};
    //             headers.forEach((header,index) => {
    //                 rowData[header] = values[index]
    //             })
    //             parsedData.push(rowData);
    //         }
    //     }

    //     setFileContent(parsedData);
    //     },[data]);

        // const offset = currentPage * itemsPerPage;
        // const currentData = fileContent.slice(offset, offset + itemsPerPage);
        // const headers = Object.keys(fileContent[0] || {});

        // const handlePageChange = ({ selected }) => {
        //     setCurrentPage(selected);
        //   }
        

          

    // const {data,error,isLoading} = useFetchFilesQuery();

    // console.log(data);
    // console.log(error)
    // console.log(new Date(data.fileDetails[0].createdAt).toLocaleString())

    // if (error) {
    //     return (
    //         <div className='container'>
    //             <div className='d-flex justify-content-start' style={{marginLeft:'80px',marginRight:"80px"}}>
    //                 <h1>{error}</h1>
    //             </div>
    //         </div>
    //     )
    // }

    // if (isLoading) {
    //     return (
    //         <div className='container'>
    //             <div className='d-flex justify-content-start' style={{marginLeft:'80px',marginRight:"80px"}}>
    //                 <h1><Skeleton/></h1>
    //             </div>
    //         </div>
    //     )
    // }

    return (
        <div className='container'>
            {/* <div>
            <table>
                <thead>
                <tr>
                    {Contentheaders.map((header, index) => (
                    <th key={index}>{header}</th>
                    ))}
                </tr>
                </thead>
                <tbody>
                {currentData.map((item, index) => (
                    <tr key={index}>
                    {headers.map((header, index) => (
                        <td key={index}>{item[header]}</td>
                    ))}
                    </tr>
                ))}
                </tbody>
            </table>

            <ReactPaginate
                previousLabel={'previous'}
                nextLabel={'next'}
                breakLabel={'......'}
                pageCount={Math.ceil(fileContent.length / itemsPerPage)}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={handlePageChange}
                containerClassName={'pagination'}
                activeClassName={'active'}
                pageClassName={'paginate-page'}
                previousClassName={'paginate-previous'}
                nextClassName={'paginate-next'}
            />
            </div> */}

            <div className='select__button__container'>
                <button type="button" className='btn btn-outline-primary select__upload__button'
                    onClick={handleAllocationStart}
                >Allocate</button>
            </div>
        </div>
      )
}
