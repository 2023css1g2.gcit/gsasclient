import React, { useState } from 'react';
import Header from './../components/Header';
import { BsPlus,BsArrowLeft, BsArrowRight } from 'react-icons/bs'; // Importing the plus icon from Bootstrap Icons
import academyImage from '../img/academy2.png';
import '../style/Academy.css';
import Switch from 'react-switch'; // Import the Switch component
import { FaMapMarkerAlt } from 'react-icons/fa';
import { useFetchAcademiesQuery } from '../store';

const initialAcademies = [
  {
    name: 'Gyalpozhing Academy',
    location: 'Mongar',
    capacity: '1000',
    image: academyImage,
    isActive:false
  },
  {
    name: 'Khotokha Academy',
    location: 'Wangdue Phodrang',
    capacity: '1500',
    image: academyImage,
    isActive:false
  },
  {
    name: 'Jamtsholing Academy',
    location: 'Samtse',
    capacity: '1200',
    image: academyImage,
    isActive:false
  },
  {
    name: 'Pemathang Academy',
    location: 'Samdrup Jongkhar',
    capacity: '1800',
    image: academyImage,
    isActive:false
  },
];

export default function AllAcademy({setAddAcademy,setEditAcademy}) {
  const [activeAcademyIndex, setActiveAcademyIndex] = useState(0);
  const [academies, setAcademies] = useState(initialAcademies);

  const {data,error,isLoading} = useFetchAcademiesQuery();
  console.log(data,error,isLoading);

  const toggleAcademyStatus = (index) => {
    const updatedAcademies = [...academies];
    updatedAcademies[index].isActive = !updatedAcademies[index].isActive;
    setAcademies(updatedAcademies);
  };

  const goToPreviousPage = () => {
    setActiveAcademyIndex((prevIndex) => (prevIndex > 0 ? prevIndex - 1 : academies.length - 1));
  };

  const goToNextPage = () => {
    setActiveAcademyIndex((prevIndex) => (prevIndex < academies.length - 1 ? prevIndex + 1 : 0));
  };

  const currentAcademy = academies[activeAcademyIndex];

  if (!currentAcademy) {
    return null; // or display a loading message or some fallback UI
  }



  return (
    <div className=''>
      <Header/>
      <div className='d-flex justify-content-between align-items-center'>
        <h3>Academy</h3>
        <div className='d-flex align-items-center'>
          <h6 className='mb-0 me-4'>Add Academy</h6>
         
          <div className=' p-2 me-4 rounded' >
              <button className='p-3 rounded h3' style={{ borderColor: "#F04A00",borderWidth:'3px', borderStyle:'solid',background:'white',color:'#F04A00'}}
              onClick={()=>setAddAcademy()}>
                <BsPlus/>
              </button>

          </div>
        </div>
      </div>
      <div className='p-3 mt-4 shadow p-3 mb-5 rounded'>
        <div className='image-container' style={{ position: 'relative', overflow: 'hidden', height: '400px' }}>
          {/* {isCampActive && (
            <div className='active-label' style={{ position: 'absolute', top: '10px', left: '10px', background: 'rgba(240, 74, 0, 0.7)', color: '#fff', padding: '5px 10px', borderRadius: '5px' }}>
              Active
            </div>
          )}
          Sliding Switch Button */}
          <div className='switch-container' style={{ position: 'absolute', top: '20px', right: '20px', display: 'flex', alignItems: 'center' }}>
            <div className='text-block'>
            <h1>{currentAcademy.name}</h1>
              <div className='d-flex mt-3'>
                <FaMapMarkerAlt className='map-marker-icon mt-1' />
                <p className='ms-2'>{currentAcademy.location}</p>
              </div>
              <p>Capacity: {currentAcademy.capacity}</p>
              <div className='mt-4'>
                 
                <button className='btn px-5'style={{backgroundColor:'#F04A00',color:"white"}}
                onClick={()=>setEditAcademy()}>Edit</button>

              </div>
            </div>
            <label htmlFor='camp-status' className='switch-label me-2 text-white'>
              Academy Status
            </label>
            <Switch
          id='camp-status'
          onChange={() => toggleAcademyStatus(activeAcademyIndex)}
          checked={currentAcademy.isActive}
          uncheckedIcon={false}
          checkedIcon={false}
          onColor='#F04A00'
          offColor='#cccccc'
          width={50}
        />
          </div>
          <div className='image-overlay'></div> {/* Container over the image */}
          <img src={currentAcademy.image} alt='' className='img-fluid rounded' />
        </div>
      </div>
      <div className='d-flex justify-content-evenly mt-4 text-center'>
      <button className='btn arrow-btn' onClick={goToPreviousPage}>
          <BsArrowLeft className='arrow-icon mx-3' />
        </button>
        <button className='btn arrow-btn' onClick={goToNextPage}>
          <BsArrowRight className='arrow-icon mx-3' />
        </button>
        </div>
    </div>
  );
}