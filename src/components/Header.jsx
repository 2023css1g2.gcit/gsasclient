
import logo from "../img/Group 119.png"

import "../style/Topheader.css"

function Dashboard() {

  const windowWidth = window.innerWidth - 295+20;
  console.log(windowWidth)

  return (
    <div className='header__container' style={{width:`${windowWidth}px`}}>
      <div  className='profile__container'>
        <div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
          <h5>Sonam</h5>
          <div className="profile__admin" style={{width: 38,height:38}}>
            ST
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;

