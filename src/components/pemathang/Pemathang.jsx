import React from "react";
// import SettingProximityDiversity from "./SettingProximityDiversity";
// import SettingRangeProximity from "./SettingRangeProximity";
// import CustomPieChart from "./SettingDiversity";
// import SettingGender from "./SettingGender";
import SettingGender from "./SettingGender/SettingGender";
import SettingProximityDiversity from './SettingProximity/SettingProximityDiversity';
import SettingRangeProximity from './SettingProximityRange/SettingRangeProximity';
import CustomPieChart from './SettingDiversity/SettingDiversity';

const Pemathang=()=>{
    return(
        <div className="p-3">
            <div className="container pt-5 shadow-lg p-3 mb-5 bg-body rounded">
                <h4>Gaylpozhing Academy</h4>
                <div className="d-flex justify-content-between mt-5">
                    <SettingProximityDiversity/>
                    <SettingRangeProximity/>
                    <SettingGender />
                </div>
                <hr />
                <CustomPieChart/>
            </div>
        </div>
    )
}
export default Pemathang;