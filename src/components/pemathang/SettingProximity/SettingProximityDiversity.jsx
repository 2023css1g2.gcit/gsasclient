import React, { useState } from 'react';
import { PieChart, Pie, Cell, Label } from 'recharts';
import './SettingProximityDiversity.css'

const SettingProximityDiversity = () => {
  const [percent, setPercent] = useState(50); // Initial value set to 50%

  // Function to calculate Proximity and Diversity values based on the percent value
  const calculateData = (percent) => {
    const diversity = 100 - percent;
    return [
      { name: 'Diversity', value: diversity, fill: '#02344F' },
      { name: 'Proximity', value: percent, fill: '#F04A00' },
    ];
  };

  const COLORS = ['#02344F', '#F04A00'];

  const handleSliderChange = (e) => {
    const newPercent = parseInt(e.target.value, 10);
    setPercent(newPercent);
  };

  // Calculate data based on the updated percent value
  const chartData = calculateData(percent);

  const calculateSliderColor = (percent) => {
    const colorStop = percent + '%';
    return `linear-gradient(to right, #F04A00 ${colorStop}, #02344F ${colorStop})`;
  };

  const sliderStyle = {
      background: calculateSliderColor(percent)
  };

  return (
    <div className='main-container'>
      <h6 className='tex'>Proximity and Diversity Intake percentage</h6>
      <div className='donut-chart-container text-center'>
        
        <PieChart width={250} height={250} style={{ marginTop: '-40px' }} margin={{ top: 0, right: 0, bottom: 0, left: 0 }}>
          <Pie
            data={chartData}
            dataKey="value"
            outerRadius={80}
            innerRadius={40}
            fill="#8884d8"
            paddingAngle={0}
            stroke="transparent"
            labelLine={false} 
            label={({
              cx,
              cy,
              midAngle,
              innerRadius,
              outerRadius,
              percent,
              index,
            }) => {
              const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
              const x = cx + radius * Math.cos(-midAngle * (Math.PI / 180));
              const y = cy + radius * Math.sin(-midAngle * (Math.PI / 180));

              return (
                <text
                  x={x}
                  y={y}
                  fill="white"
                  textAnchor="middle"
                  dominantBaseline="central"
                  fontSize={14}
                >
                  {`${(percent * 100).toFixed(0)}%`}
                </text>
              );
            }}
          >
            {chartData.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
            <Label width={30} position="inside" fill="#FFFFFF" />
          </Pie>
        </PieChart>
        <div className='legend-container'>
          <div className='d-flex justify-content-start ms-2'>
            <div style={{ height: '20px', width: '20px', backgroundColor: '#F04A00' }}></div>
            <p className=''>Proximity</p>
          </div>
          <div className='d-flex justify-content-start ms-2'>
            <div style={{ height: '20px', width: '20px', backgroundColor: '#02344F' }}></div>
            <p className=''>Diversity</p>
          </div>
        </div>
      </div>
      <div className='slider-container text-center'>
        <div className='container  d-flex justify-content-center align-item-center'>
          < p className=''>0%</p>
          <input 
            type='range'
            min='0'
            max='100'
            value={percent}
            onChange={handleSliderChange}
            style={sliderStyle}
            className='slider w-100 mt-1 ms-2'
          />
          <p className='ms-2'>100%</p>

        </div>
      </div>
      
    </div>
  );
};

export default SettingProximityDiversity;