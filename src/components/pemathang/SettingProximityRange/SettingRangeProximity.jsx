import React, { useState } from 'react';
import { PieChart, Pie, Cell } from 'recharts';


const SettingRangeProximity = () => {
    const [sliderValue, setSliderValue] = useState(5); // Initial value for the slider

    const centerX = 125;
    const centerY = 125;

    const handleSliderChange = (event) => {
        const newSliderValue = parseInt(event.target.value, 10);
        setSliderValue(newSliderValue);
    };

    const data = [
        { name: 'RangeIntake', value: sliderValue },
        { name: 'RangeLeft', value: 10 - sliderValue } // Ensuring the total value remains 10
    ];

    const COLORS = ['#FFBF00', 'rgba(45, 156, 219, 0.15)'];

    const calculateSliderColor = () => {
        const percent = (sliderValue / 10) * 100;
        return {
            background: `linear-gradient(to right, #FFBF00 ${percent}%, #D9D9D9 ${percent}%)`,
            thumb: `rgb(${255 - sliderValue * 15}, ${191 + sliderValue * 6}, 0)`,
        };
    };

    const sliderStyles = calculateSliderColor();


    return (
        <div className='main-container'>
            <h6 className='text-center'>Porximtiy Range Intake</h6>
            <div className='donut-chart-container text-center'>
                <PieChart width={250} height={250} style={{ marginTop: '-30px' }}>
                    <text x={centerX} y={centerY} fill="#000" textAnchor="middle" dominantBaseline="central" fontSize={20}>
                        1 - {data[0].value}
                    </text>
                    <Pie
                        data={data}
                        dataKey="value"
                        outerRadius={80}
                        innerRadius={40}
                        fill="#8884d8"
                        paddingAngle={0}
                        stroke="transparent"
                        labelLine={false}

                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index]} />
                        ))}
                    </Pie>
                </PieChart>
            </div>
            <div className='slider-container text-center'>
                <div className='container  d-flex justify-content-center align-item-center'>
                    < p className=''>1</p>
                    <input
                        type="range"
                        min="1"
                        max="10" 
                        value={sliderValue}
                        onChange={handleSliderChange}
                        className="slider ms-2  mt-1"style={sliderStyles}
                    />
                    <p className='ms-1'>10</p>

                </div>
            </div>
        </div>
    );
};

export default SettingRangeProximity;