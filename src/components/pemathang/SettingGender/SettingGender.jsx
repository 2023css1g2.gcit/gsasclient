import React, { useState } from 'react';

const SettingGender = () => {
    const [maleCount, setMaleCount] = useState('');
    const [femaleCount, setFemaleCount] = useState('');

    const handleMaleCountChange = (e) => {
        setMaleCount(e.target.value);
    };

    const handleFemaleCountChange = (e) => {
        setFemaleCount(e.target.value);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        // Do something with maleCount and femaleCount values, for example, send them to an API or perform further processing
        console.log('Male Count:', maleCount);
        console.log('Female Count:', femaleCount);
    };

    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <h6>Capacity: 1000</h6>
            <form action="" className="mt-4" onSubmit={handleSubmit}>
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                    />
                </div>
                <div className="d-flex flex-column mt-5">
                    <label htmlFor="femaleCount">Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                    />
                </div>
            </form>
        </div>
    );
};

export default SettingGender;