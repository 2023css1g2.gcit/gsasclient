import React, { useState } from 'react';
import { PieChart, Pie, Cell } from 'recharts';
import './SettingDiversity.css';
import { useDispatch,useSelector } from 'react-redux';
import { setCentral,setSouth,setWest } from '../../../store';
import { useEffect } from 'react';

const COLORS = ['#02344F', '#EEB200', '#F04A00'];

const CustomPieChart = () => {

    const dispatch = useDispatch();
    const [proximity,setProximity] = useState();
    const [percentage,setPercentage] = useState();

    const gyalpozhingData = useSelector((state)=> state.gyalpozhing)

    console.log("from diversity ", gyalpozhingData);

    useEffect(() => {
        setProximity(gyalpozhingData.proximity)
        if (proximity) {
            setPercentage(100-proximity);
        }
    },[gyalpozhingData])

    console.log("Promity ",proximity);

    const [sliderValues, setSliderValues] = useState({
        South: 0,
        Central: 0,
        West: 0,
    });

    const handleSliderChange = (name, value) => {
        // Calculate remaining value after the current slider change
        const remainingValue = percentage - value;

        // Distribute the remaining value evenly to other sliders
        const updatedValues = {
            South: name === 'South' ? value : remainingValue / 2,
            Central: name === 'Central' ? value : remainingValue / 2,
            West: name === 'West' ? value : remainingValue / 2,
        };

        setSliderValues(updatedValues);
        dispatch(setCentral(parseInt(sliderValues.Central)))
        dispatch(setWest(parseInt(sliderValues.West)))
        dispatch(setSouth(parseInt(sliderValues.South)));
    };

    const data = [
        { name: 'South', value: sliderValues.South, fill: COLORS[0] },
        { name: 'Central', value: sliderValues.Central, fill: COLORS[1] },
        { name: 'West', value: sliderValues.West, fill: COLORS[2] },
    ];
    

    return (
        <div className={`${gyalpozhingData.proximity === 100 ?'d-none':''}`}>
            <h6 className='ms-5 ps-5'>Diversity Intake Percentage</h6>
            <div className='d-flex justify-content-evenly align-item-center' style={{marginTop:"-5%"}}>
            
                <PieChart width={400} height={400}> 
                    <Pie 
                        data={data}
                        dataKey="value"
                        nameKey="name"
                        outerRadius={80}
                        fill="#8884d8"
                        labelLine={false}
                        label={({
                            cx,
                            cy,
                            midAngle,
                            innerRadius,
                            outerRadius,
                            percent,
                            index,
                        }) => {
                            const RADIAN = Math.PI / 180;
                            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
                            const x = cx + radius * Math.cos(-midAngle * RADIAN);
                            const y = cy + radius * Math.sin(-midAngle * RADIAN);

                            return (
                                <text
                                    x={x}
                                    y={y}
                                    fill="#FFFFFF"
                                    textAnchor="middle"
                                    dominantBaseline="middle"
                                >
                                    {`${data[index].name}`}
                                </text>
                            );
                        }}
                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
                <div className='d-flex flex-column align-item-center justify-content-center me-5 px-5'style={{width:"35%"}}>
                    <h4>East 0%</h4>
                    <div className='d-flex '>
                        <label>{parseInt(sliderValues.South)}%</label>
                        <input 
                            className='w-100 slider1 mt-1'
                            type="range"
                            min="0"
                            max="30"
                            value={sliderValues.South}
                            onChange={(e) => handleSliderChange('South', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[0] }} // Set background color to #02344F
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}%`:"100%"}</label>
                    </div>
                    <div className='d-flex mt-2  w-100'>
                        <label>{parseInt(sliderValues.Central)}%</label>
                        <input
                            className='w-100 slider2 mt-1'
                            type="range"
                            min="0"
                            max="30"
                            value={sliderValues.Central}
                            onChange={(e) => handleSliderChange('Central', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[1], backgroundColor: '#EEB200' }} // Set background color to #EEB200
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}%`:"100%"}</label>
                    </div>
                    <div className='d-flex mt-2'>
                        <label>{parseInt(sliderValues.West)}%</label>
                        <input
                            className='w-100 slider3  mt-1'
                            type="range"
                            min="0"
                            max="30"
                            
                            value={sliderValues.West}
                            onChange={(e) => handleSliderChange('West', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[2] }} // Set background color to #F04A00
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}%`:"0%"}</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CustomPieChart;
