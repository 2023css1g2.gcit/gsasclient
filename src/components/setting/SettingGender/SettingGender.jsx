import React, { useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { setNumberOfFemale,setNumberOfMale } from '../../../store';

const SettingGender = () => {

    const dispatch = useDispatch();

    const gyalpozhingData = useSelector((state) => state.gyalpozhing);

    console.log("from gender ",gyalpozhingData);

    const [maleCount, setMaleCount] = useState('');
    const [femaleCount, setFemaleCount] = useState('');

    const handleMaleCountChange = (e) => {
        setMaleCount(e.target.value);
        dispatch(setNumberOfMale(parseInt(e.target.value)));
    };

    const handleFemaleCountChange = (e) => {
        setFemaleCount(e.target.value);
        dispatch(setNumberOfFemale(parseInt(e.target.value)));
    };

    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <h6>Capacity: 1000</h6>
            <form action="" className="mt-4">
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                    />
                </div>
                <div className="d-flex flex-column mt-5">
                    <label htmlFor="femaleCount">Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                    />
                </div>
            </form>
        </div>
    );
};

export default SettingGender;