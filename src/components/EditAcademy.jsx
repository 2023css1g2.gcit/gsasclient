import React, { useState, useRef } from 'react';
import Header from './../components/Header';
import '../style/AddAcademy.css';
import { Link } from 'react-router-dom';
import { BsArrowLeft } from 'react-icons/bs'; // Importing the plus icon from Bootstrap Icons

export default function EditAcademy({backToAcademy}) {
  const [selectedFile, setSelectedFile] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [dzongkhag, setDzongkhag] = useState('');
  const [name, setName] = useState('');
  const [capacity, setCapacity] = useState('');
  const fileInputRef = useRef(null);

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleCapacityChange = (e) => {
    setCapacity(e.target.value);
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);

    // Preview the selected image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      setPreviewImage(null);
    }
  };

  const handleDrop = (e) => {
    e.preventDefault();
    const file = e.dataTransfer.files[0];
    setSelectedFile(file);

    // Preview the dropped image
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage(reader.result);
    };
    if (file) {
      reader.readAsDataURL(file);
    } else {
      setPreviewImage(null);
    }
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

  const handleBrowseClick = () => {
    fileInputRef.current.click();
  };

  const handleDzongkhagChange = (e) => {
    setDzongkhag(e.target.value);
  };
  return (
    <div>
      <Header />
      <h4 className='mt-4'>Edit Academy</h4>
      <div className='mt-4 shadow-lg p-3 mb-5 bg-white rounded'>
        <form action=''>
          <div className='row'>
            <div className='col-md-6 p-5'>
              {/* ... other input fields */}
              <div className='spacing'>
                <div className='input1'>
                    <label htmlFor="">Name</label>
                    <input type="text" name='name' value={name} onChange={handleNameChange} required className='form-control mt-1' />
                </div>
                <div className='input2 mt-4'>
                    <div className='row'>
                        <div className='col-md-6 col-12'>
                            <div className='sub-input-1'>
                                <label htmlFor="">Dzongkhag</label>
                                <select
                                    className='form-control mt-1'
                                    value={dzongkhag}
                                    onChange={handleDzongkhagChange}
                                    required
                                >
                                    <option value='' disabled>Select Dzongkhag</option>
                                    <option value='Dzongkhag 1'>Dzongkhag 1</option>
                                    <option value='Dzongkhag 2'>Dzongkhag 2</option>
                                    {/* ... add more options */}
                                    <option value='Dzongkhag 20'>Dzongkhag 20</option>
                                </select>
                            </div>
                        </div>
                        <div className='col-md-6 col-12'>
                            <div className='sub-input-2'>
                            <label htmlFor="">Capacity</label>
                            <input type="text" name='capacity' value={capacity} onChange={handleCapacityChange} required className='form-control mt-1' />
                            </div>
                        </div>

                        <div className='mt-5'>
                            <div className='mt-5 py-3 px-2'>
                            <div onClick={()=>backToAcademy()} style={{textDecoration:'none',cursor:'pointer'}} className='text-dark d-flex'>
                                <BsArrowLeft className='arrow-icon mx-2 h3' /><p className='text-dark'>Go back to Academy</p>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
             <div className='col-md-5 p-5'>
              <div className='spacing'>
                <div className='input1 shadow'>
                  <div
                    className='drop-zone'
                    onDrop={handleDrop}
                    onDragOver={handleDragOver}
                    onClick={handleBrowseClick}
                  >
                      <div>Drag & drop <br /> a file here, or click to select one</div>
                    <input
                      type='file'
                      className='form-control'
                      onChange={handleFileChange}
                      style={{ display: 'none' }}
                      ref={fileInputRef}
                    />
                  </div>
                </div>
              </div>
              <div className='mt-3 shadow preview-image text-center'>
                    <div className='preview-box'>
                        <img src={previewImage} alt='Preview' className='img-fluid' style={{height:"200px"}} />
                      </div>
              </div>
              <div className='row mt-5'>
                <div className='col-md-6'>
                  <button className='btn  form-control arrow-btn border-danger '>Reset</button>
                </div>
                <div className='col-md-6'>
                  <button className='btn  form-control arrow-btn border-danger' >Upadte</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}