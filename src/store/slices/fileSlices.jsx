import { createSlice } from "@reduxjs/toolkit";

const fileSlice = createSlice({
    name: 'file',
    initialState: {
        name:'',
        fileContent: [],
        year: '',
        studentData:[]
    }, 
    reducers: {
        updateData: (state,action) => {
            return {
                name: action.payload.fileName,
                year: action.payload.year,
                fileContent:action.payload.fileContent,
                studentData: []
            }
        },
        addStudentData: (state,action) => {
            state.studentData = [action.payload];
        }
    }
})

export const {updateData,addStudentData} = fileSlice.actions;
export const fileReducer = fileSlice.reducer;