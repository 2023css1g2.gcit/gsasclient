import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    academy:'Gyalpozhing Academy',
    proximity: 70,
    proximityRange:5,
    east:0,
    central:10,
    south:10,
    west:10,
    numberOfFemale:500,
    numberOfMale:500
}

const gyalpozhingSlice = createSlice({
    name:'gyalpozhing',
    initialState,
    reducers: {
        setAcademy: (state,action) => {
            state.academy = action.payload;
        },
        setProximity: (state,action) => {
            state.proximity = action.payload;
        },
        setProximityRange: (state,action) => {
            state.proximityRange = action.payload;
        },
        setCentral: (state, action) => {
            state.central = action.payload;
        },
        setSouth: (state, action) => {
        state.south = action.payload;
        },
        setWest: (state, action) => {
        state.west = action.payload;
        },
        setNumberOfFemale: (state, action) => {
        state.numberOfFemale = action.payload;
        },
        setNumberOfMale: (state, action) => {
        state.numberOfMale = action.payload;
        },
    }
})

export const {setAcademy,setProximity,setProximityRange,setCentral,setSouth,setWest,setNumberOfFemale,setNumberOfMale} = gyalpozhingSlice.actions;
export const gyalpozhingReducer = gyalpozhingSlice.reducer;