import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { fileApi } from "./apis/fileApi";
import {academyApi} from './apis/academyApi';
import { setupListeners } from "@reduxjs/toolkit/query";
import {profileApi} from './apis/profileApi';
import { fileReducer,updateData,addStudentData } from "./slices/fileSlices";
import { gyalpozhingReducer,setAcademy,setCentral,setNumberOfFemale,setNumberOfMale,setProximity,setProximityRange,setSouth,setWest } from "./slices/gyalpozhing";
import { allocationApi } from "./apis/allocationApi";
import { enlisteeApi } from "./apis/enlisteeApi";

export const store = configureStore({
    reducer: {
        [fileApi.reducerPath]:fileApi.reducer,
        [academyApi.reducerPath]:academyApi.reducer,
        [profileApi.reducerPath]:profileApi.reducer,
        [allocationApi.reducerPath]:allocationApi.reducer,
        [enlisteeApi.reducerPath]:enlisteeApi.reducer,
        file: fileReducer,
        gyalpozhing: gyalpozhingReducer
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware()
            .concat(fileApi.middleware)
            .concat(academyApi.middleware)
            .concat(profileApi.middleware)
            .concat(allocationApi.middleware)
            .concat(enlisteeApi.middleware);
    }
})

setupListeners(store.dispatch);

export {useFetchFilesQuery} from './apis/fileApi';
export {useFetchAcademiesQuery} from './apis/academyApi';
export {useStartAllocationMutation} from './apis/allocationApi';
export {useAddStudentMutation} from './apis/enlisteeApi';
export {useFetchAdminQuery,useChangePasswordMutation} from './apis/profileApi';

export {updateData,setAcademy,setCentral,setNumberOfFemale,setNumberOfMale,setProximity,setProximityRange,setSouth,setWest,addStudentData};