import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const academyApi = createApi({
    reducerPath:'academy',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchAcademies: builder.query({
                query: () => {
                    return {
                        url: '/academy',
                        method: 'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchAcademiesQuery} = academyApi;
export {academyApi};