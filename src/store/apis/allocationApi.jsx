import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const allocationApi = createApi({
    reducerPath:'allocation',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization',`Bearer ${token}`);
            return headers;
        }
    }),
    endpoints(builder) {
        return {
            startAllocation: builder.mutation({
                query:(allocationData) => ({
                    url: 'startAllocation',
                    method:'POST',
                    body: allocationData,
                })
            })
        }
    }
})

export const {useStartAllocationMutation} = allocationApi;
export {allocationApi};