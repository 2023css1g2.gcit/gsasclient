import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const fileApi = createApi({
    reducerPath:'files',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchFiles: builder.query({
                query: () => {
                    return {
                        url:'/getAllCSVFile',
                        method:'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchFilesQuery} = fileApi;
export {fileApi};