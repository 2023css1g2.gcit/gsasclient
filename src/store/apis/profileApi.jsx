import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const profileApi = createApi({
    reducerPath: 'profile',
    baseQuery: fetchBaseQuery({
        baseUrl:'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchAdmin: builder.query({
                query: () => {
                    return {
                        url: '/getAdmin',
                        method:'GET'
                    }
                }
            }),
            changePassword: builder.mutation({
                query: (passwordData) => {
                    return {
                        url: '/changePassword',
                        method: 'POST',
                        body: passwordData
                    }
                }
            })
        }
    }
})

export const {useFetchAdminQuery,useChangePasswordMutation} = profileApi;
export {profileApi};