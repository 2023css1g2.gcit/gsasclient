import Overview from "../components/Profile/Overview";
import ChangePasswordProfile from "../components/Profile/ChangePasswordProfile";
import { useState } from "react";

const Profile = () => {

  const [currentPage,setCurrentPage] = useState(true);

  const handlePageChange = () => {
    setCurrentPage(!currentPage);
  }

  console.log("current page from profile ",currentPage);

  return (
    <div>
      {currentPage && currentPage === true ? (
        <Overview handlePageChange = {handlePageChange} currentPage = {currentPage}/>
      ):(
        <ChangePasswordProfile handlePageChange = {handlePageChange} currentPage = {currentPage}/>
      )

      }
    </div>
  );
};

export default Profile;
