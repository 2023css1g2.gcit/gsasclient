import React, { useRef, useState } from 'react';
import logo from '../img/desung logo 1.png';
import { AiFillEyeInvisible } from 'react-icons/ai';
import '../style/login.css';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';

function Login({handleAdminLogin}) {

  const navigate = useNavigate();

  const checkboxRef = useRef(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [error,setError] = useState('');

  const handleCheckboxChange = () => {
    const isChecked = checkboxRef.current.checked;
    console.log(isChecked);
  };

  const handleLogin = async (e) => {
    e.preventDefault();
    setIsLoading(true)

    try {
      const response = await fetch('http://localhost:8000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data.token);
        const token = data.token;

        Cookies.set('auth_token',token,{expires:7});

        setError('');
        handleAdminLogin();
      } else {
        const data = await response.json();
        setError(data.message);
      }
    } catch (error) {
      console.error('Error ', error.message);
    } finally {
      setIsLoading(false);
    }
  };

  console.log(email);
  console.log(password);

  return (
    <div className="container d-flex justify-content-center align-items-center min-vh-100">
      <div className="login__innercontainer text-center" style={{ width: '626px' }}>
        <div className="login__login bg-white rounded p-3 shadow">
          <img src={logo} alt="logo" style={{ width: '100px', height: '100px', marginTop: '15px' }} />
          <h4>Welcome!</h4>
          
          <form className="login__form" onSubmit={handleLogin}>
            {error &&
              <div className="login__error__message">
                <p className=''>{error}</p>
              </div>
            }
            <div className="">
              <label htmlFor="Email">Email</label>
              <div className="login__text__area">
                <input
                  type="email"
                  id="Email"
                  name="Email"
                  className="login__text__input"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
            </div>
            <div className="">
              <label htmlFor="password">Password</label>
              <div className="login__text__area">
                <input
                  type="password"
                  id="password"
                  name="password"
                  className="login__text__input"
                  onChange={(e) => setPassword(e.target.value)}
                />
                <AiFillEyeInvisible size={20} className="login__eye__closed" />
              </div>
            </div>
            <div className="affirmation__Container d-flex justify-content-between">
              <div className="login__remember__container d-flex align-items-center">
                <input
                  type="checkbox"
                  ref={checkboxRef}
                  onChange={handleCheckboxChange}
                  className="mr-2"
                />
                <span className="login__remember">Remember me</span>
              </div>
              <a className='login__link text-decoration-none' href="/forgotpassword">Forgot password?</a>
            </div>
            <div style={{ textAlign: 'center' }}>
              {isLoading ? (
                <button className="login__btn" disabled>
                  <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                </button>
              ):(
                <input
                type="submit"
                value="Login"
                className="login__btn"
              />
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
