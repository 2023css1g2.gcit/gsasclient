import React, { useEffect, useState } from 'react'
import { Stepper } from 'react-form-stepper';
import Cookies from 'js-cookie';
import Select from '../components/allocation/Select';
import Preview from '../components/allocation/Preview';
import Setting from '../components/allocation/Setting';
import PreviewResult from '../components/allocation/PreviewResult';
import Output from '../components/allocation/Output';
import Header from '../components/Header';

export default function Allocation() {

  const [activeStep,setActiveStep] = useState(0);

  const steps = [
    { label: 'Select' },
    { label: 'Preview' },
    {label:'Setting'},
    {label:'Preview Result'},
    {label:'Output'}
  ];

  const setNextStep = () => {
    setActiveStep(activeStep + 1);
  }

  const setPreviousStep = () => {
    setActiveStep(activeStep - 1);
  }

  function getSectionComponent() {
    switch (activeStep) {
      case 0:
        return <Select
          setNextStep = {setNextStep}
          setPreviousStep = {setPreviousStep}
        />;
      case 1:
        return <Preview 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
        />;
      case 2:
        return <Setting 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
        />;
      case 3:
        return <PreviewResult 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
        />;
      case 4:
        return <Output 
          setPreviousStep = {setPreviousStep}
          setNextStep = {setNextStep}
        />;
      default:
        return null;
    }
  }

  return (
    <div>
      <div className=''>
        <Header/>
      </div>
      <div style={{ padding: '0 10px 10px 10px'}}>
      <div style={{marginTop:'100px'}}>
          <Stepper
            steps={steps}
            activeStep={activeStep}
            className="sticky-stepper"
            style={{
              position: 'fixed',
              top: '80px',
              left: '267px', // Use the desired left value in pixels
              right: '10px', // Use the desired right value in pixels
              zIndex: 1
            }}
          />
        </div>
        <div style={{ padding: '20px' }}>
          {getSectionComponent()}
        </div>
      </div>
    </div>
  )
}
