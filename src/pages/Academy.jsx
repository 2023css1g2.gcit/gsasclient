import React from 'react';
import AllAcademy from '../components/AllAcademy';
import AddAcademy from '../components/AddAcademy';
import { useState } from 'react';
import EditAcademy from '../components/EditAcademy';

export default function Academy() {

  const [isAddAcademy,setIsAddAcademy] = useState(false);
  const [isEditAcademy,setIsEditAcademy] = useState(false);

  function setAddAcademy(){
    setIsAddAcademy(true);
    setIsEditAcademy(false);
  }

  function setEditAcademy(){
    setIsAddAcademy(false);
    setIsEditAcademy(true);
  }

  function backToAcademy(){
    setIsAddAcademy(false);
    setIsEditAcademy(false);
  }

  return (
    <>
      {!isAddAcademy && !isEditAcademy &&
        <AllAcademy setAddAcademy={setAddAcademy} setEditAcademy={setEditAcademy}/>
      }
      {isAddAcademy && !isEditAcademy &&
        <AddAcademy backToAcademy={backToAcademy}/>
      }

      {isEditAcademy && !isAddAcademy &&
        <EditAcademy backToAcademy={backToAcademy}/>
      }
    </>
  )
}
